package runner

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	gitlabutils "gitlab.com/gitlab-org/gl-openshift/gitlab-runner-operator/controllers/utils"
	"gopkg.in/yaml.v2"
)

type imageName string

const (
	// GitLabRunnerImage has name of runner
	GitLabRunnerImage = imageName("gitlab-runner")
	// GitLabRunnerHelperImage has name of runner helper
	GitLabRunnerHelperImage = imageName("gitlab-runner-helper")
	// releaseFile is the location where metadata about releases is kept. Look at hack/assets
	releaseFile = "release.yaml"
)

//go:generate mockery --name=imageResolver --inpackage
type imageResolver interface {
	HelperImage() (string, error)
	RunnerImage() (string, error)
}

type releaseDirImageResolver struct {
}

func (r *releaseDirImageResolver) getImageURL(image imageName) (string, error) {
	release, err := getRelease()
	if err != nil {
		return "", fmt.Errorf("get release: %w", err)
	}

	var targetContainer *Image
	for _, container := range release.Images {
		if container.Name == string(image) {
			targetContainer = &container
			break
		}
	}

	if targetContainer == nil {
		return "", fmt.Errorf("image %s not found in release file", image)
	}

	return targetContainer.Image(), nil
}

func (r *releaseDirImageResolver) HelperImage() (string, error) {
	return r.getImageURL(GitLabRunnerHelperImage)
}

func (r *releaseDirImageResolver) RunnerImage() (string, error) {
	return r.getImageURL(GitLabRunnerImage)
}

// Image represents a
// single microservice image
type Image struct {
	Name      string `json:"name" yaml:"name"`
	Upstream  string `json:"upstream" yaml:"upstream"`
	Certified string `json:"certified" yaml:"certified"`
}

// Image function auto-selects image to deploy
func (img Image) Image() string {
	if img.Certified != "" && gitlabutils.IsOpenshift() {
		return img.Certified
	}

	return img.Upstream
}

// Release defines a GitLab release
type Release struct {
	Version string  `json:"version" yaml:"version"`
	Images  []Image `json:"images" yaml:"images"`
}

// getRelease reads the release file and returns a release object
func getRelease() (*Release, error) {
	releaseFilePath, err := findReleaseFile()
	if err != nil {
		return nil, fmt.Errorf("get releaseFilePath: %w", err)
	}

	releaseFile, err := ioutil.ReadFile(releaseFilePath)
	if err != nil {
		return nil, fmt.Errorf("read release file: %w", err)
	}

	release := &Release{}
	err = yaml.Unmarshal(releaseFile, release)
	if err != nil {
		return nil, fmt.Errorf("unmarshal release file: %w", err)
	}

	return release, nil
}

var rootReleaseFileProvider = func() string {
	return filepath.Join("/", releaseFile)
}

var projectReleaseFileProvider = func(basePath string) string {
	return filepath.Join(basePath, "hack/assets", releaseFile)
}

func findReleaseFile() (string, error) {
	// running in cluster
	clusterReleasePath := rootReleaseFileProvider()
	if _, err := os.Stat(clusterReleasePath); err == nil {
		return clusterReleasePath, nil
	}

	wd, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("get working directory: %w", err)
	}

	var basePath string
	if strings.HasSuffix(wd, "gitlab-runner-operator") {
		// running locally in a project
		basePath = wd
	} else if strings.Contains(wd, "gitlab-runner-operator") {
		// check if path is project dir is in path but not last dir
		basePath = strings.SplitAfter(wd, "gitlab-runner-operator")[0]
	} else {
		return "", fmt.Errorf("failed to find release file")
	}

	projectReleasePath := projectReleaseFileProvider(basePath)
	if _, err := os.Stat(projectReleasePath); err == nil {
		return projectReleasePath, nil
	}

	return "", fmt.Errorf("failed to find release file")
}
