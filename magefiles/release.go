package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"text/template"

	"github.com/magefile/mage/mg"
)

// Release defines the set of commands used for a release.
type Release mg.Namespace

const commitTemplate = `
operator gitlab-runner-operator ({{ .Version }})

Signed-off-by: {{ .GitUsername }} <{{ .GitEmail }}>
`

// OperatorGitHub defines the set of commands used for the operator release on GitHub.
func (Release) OperatorGitHub(
	version, email, username, ghToken, ghRepoNamespace,
	upstreamProjectNamespace, ghProject string,
) error {
	for _, cmd := range []string{
		"git config --global user.email " + strconv.Quote(email),
		"git config --global user.name " + strconv.Quote(username),
		"git config --global hub.protocol https",
	} {
		if err := NewCommand().Cmd(cmd).Run(); err != nil {
			return err
		}
	}

	cwd, _ := os.Getwd()
	if _, err := os.Stat(ghProject); err != nil && os.IsNotExist(err) {
		if err := NewCommand().Cmd("hub clone %s/%s", ghRepoNamespace, ghProject).Run(); err != nil {
			return err
		}
	}

	projectPwd := filepath.Join(cwd, ghProject)
	pwdCommandBuilder := NewCommand().Dir(projectPwd)

	if err := pwdCommandBuilder.
		IgnoreErrs("remote upstream already exists").
		Cmd("hub remote add upstream %s/%s", upstreamProjectNamespace, ghProject).
		Run(); err != nil {
		return err
	}

	versionNoPrefix := version[1:]
	if err := pwdCommandBuilder.Cmd("git fetch upstream").Run(); err != nil {
		return err
	}

	branch := "gitlab-runner-operator-" + versionNoPrefix
	if err := pwdCommandBuilder.IgnoreErrs("already exists").Cmd("hub checkout -b %s upstream/main", branch).Run(); err != nil {
		return err
	}

	if err := pwdCommandBuilder.Cmd("hub checkout %s", branch).Run(); err != nil {
		return err
	}

	operatorGhPath := filepath.Join("operators", "gitlab-runner-operator", versionNoPrefix)
	fullOperatorGhPath := filepath.Join(cwd, ghProject, operatorGhPath)

	if err := NewCommand().Cmd("mkdir -p %s", fullOperatorGhPath).Run(); err != nil {
		return err
	}

	var parentBundleDir string
	switch ghProject {
	case "community-operators":
		parentBundleDir = "community-operator/" + versionNoPrefix
	default:
		parentBundleDir = "certification"
	}

	commitMsg, err := compileCommitTemplate(version, username, email)
	if err != nil {
		return err
	}

	pushURL := fmt.Sprintf("https://%s@github.com/%s/%s.git", ghToken, ghRepoNamespace, ghProject)

	for _, cmd := range []*Command{
		pwdCommandBuilder.Cmd("cp -r %s/. %s", filepath.Join(cwd, parentBundleDir), fullOperatorGhPath),
		pwdCommandBuilder.Cmd("git add %s", operatorGhPath),
		pwdCommandBuilder.IgnoreErrs("nothing to commit").In(bytes.NewBufferString(commitMsg)).Cmd("git commit -F -"),
		pwdCommandBuilder.Cmd("git push %s %s -f", pushURL, branch),
	} {
		if err := cmd.Run(); err != nil {
			return err
		}
	}

	prTitle := fmt.Sprintf("operator gitlab-runner-operator (%s)", versionNoPrefix)
	prHead := fmt.Sprintf("%s/%s:%s", ghRepoNamespace, ghProject, branch)

	return pwdCommandBuilder.
		IgnoreErrs("A pull request already exists").
		Cmd("hub pull-request -m %s --head %s --base main", strconv.Quote(prTitle), strconv.Quote(prHead)).
		Run()
}

func compileCommitTemplate(version, username, email string) (string, error) {
	tmpl, err := template.New("commit-template").Parse(commitTemplate)
	if err != nil {
		return "", err
	}

	var b bytes.Buffer
	if err := tmpl.Execute(&b, struct {
		Version     string
		GitUsername string
		GitEmail    string
	}{
		Version:     version,
		GitUsername: username,
		GitEmail:    email,
	}); err != nil {
		return "", err
	}

	return b.String(), nil
}
